; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;


; Audio registers - relative to A64_AC

DA_CTL                  * &000
DA_FAT0                 * &004
DA_FAT1                 * &008
DA_ISTA                 * &00C
DA_RXFIFO               * &010
DA_FCTL                 * &014
DA_FSTA                 * &018
DA_INT                  * &01C
DA_TXFIFO               * &020
DA_CLKD                 * &024
DA_TXCNT                * &028
DA_RXCNT                * &02C
DA_TXCHSEL              * &030
DA_TXCHMAP              * &034
DA_RXCHSEL              * &038
DA_RXCHMAP              * &03C

Codec_RST               * &200
SYSCLK_CTL              * &20C
MOD_CLK_ENA             * &210
MOD_RST_CTL             * &214
SYS_SR_CTRL             * &218
SYS_SRC_CLK             * &21C
SYS_DVC_MOD             * &220

AIF1_CLK_CTRL           * &240
AIF1_ADCDAT_CTRL        * &244
AIF1_DACDAT_CTRL        * &248
AIF1_MIXR_SRC           * &24C
AIF1_VOL_CTRL1          * &250
AIF1_VOL_CTRL2          * &254
AIF1_VOL_CTRL3          * &258
AIF1_VOL_CTRL4          * &25C
AIF1_MXR_GAIN           * &260
AIF1_RXD_CTRL           * &264

AIF2_CLK_CTRL           * &280
AIF2_ADCDAT_CTRL        * &284
AIF2_DACDAT_CTRL        * &288
AIF2_MIXR_SRC           * &28C
AIF2_VOL_CTRL1          * &290
AIF2_VOL_CTRL2          * &294
AIF2_VOL_CTRL3          * &298
AIF2_VOL_CTRL4          * &29C
AIF2_MXR_GAIN           * &2A0
AIF2_RXD_CTRL           * &2A4

AIF3_CLK_CTRL           * &2C0
AIF3_ADCDAT_CTRL        * &2C4
AIF3_DACDAT_CTRL        * &2C8
AIF3_SGP_CTRL           * &2CC
AIF3_RXD_CTRL           * &2E4

ADC_DIG_CTRL            * &300
ADC_VOL_CTRL            * &304
ADC_DBG_CTRL            * &308

HMIC_CTRL1              * &310
HMIC_CTRL2              * &314
HMIC_STS                * &318

DAC_DIG_CTRL            * &320
DAC_VOL_CTRL            * &324
DAC_DBG_CTRL            * &328
DAC_MXR_SRC             * &330
DAC_MXR_GAIN            * &334

AC_ADC_DAPLSTA          * &400
AC_ADC_DAPRSTA          * &404
AC_ADC_DAPLCTRL         * &408
AC_ADC_DAPRCTRL         * &40C
AC_ADC_DAPLTL           * &410
AC_ADC_DAPRTL           * &414
AC_ADC_DAPLHAC          * &418
AC_ADC_DAPLLAC          * &41C
AC_ADC_DAPRHAC          * &420
AC_ADC_DAPRLAC          * &424
AC_ADC_DAPLDT           * &428
AC_ADC_DAPLAT           * &42C
AC_ADC_DAPRDT           * &430
AC_ADC_DAPRAT           * &434
AC_ADC_DAPNTH           * &438
AC_ADC_DAPLHNAC         * &43C
AC_ADC_DAPLLNAC         * &440
AC_ADC_DAPRHNAC         * &444
AC_ADC_DAPRLNAC         * &448

AC_DAPHHPFC             * &44C
AC_DAPLHPFC             * &450
AC_DAPOPT               * &454

AC_DAC_DAPCTRL          * &480

ARG_ENA                 * &4D0
DRC_ENA                 * &4D4

SRC_BISTCR              * &4D8
SRC_BISTST              * &4DC
SRC1_CTRL1              * &4E0
SRC1_CTRL2              * &4E4
SRC1_CTRL3              * &4E8
SRC1_CTRL4              * &4EC
SRC2_CTRL1              * &4F0
SRC2_CTRL2              * &4F4
SRC2_CTRL3              * &4F8
SRC2_CTRL4              * &4FC

AC_DRC0_HHPFC           * &600
AC_DRC0_LHPFC           * &604
AC_DRC0_CTRL            * &608
AC_DRC0_LPFHAT          * &60C
AC_DRC0_LPFLAT          * &610
AC_DRC0_RPFHAT          * &614
AC_DRC0_RPFLAT          * &618
AC_DRC0_LPFHRT          * &61C
AC_DRC0_LPFLRT          * &620
AC_DRC0_RPFHRT          * &624
AC_DRC0_RPFLRT          * &628
AC_DRC0_LRMSHAT         * &62C
AC_DRC0_LRMSLAT         * &630
AC_DRC0_RRMSHAT         * &634
AC_DRC0_RRMSLAT         * &638
AC_DRC0_HCT             * &63C
AC_DRC0_LCT             * &640
AC_DRC0_HKC             * &644
AC_DRC0_LKC             * &648
AC_DRC0_HOPC            * &64C
AC_DRC0_LOPC            * &650
AC_DRC0_HLT             * &654
AC_DRC0_LLT             * &658
AC_DRC0_HKI             * &65C
AC_DRC0_LKI             * &660
AC_DRC0_HOPL            * &664
AC_DRC0_LOPL            * &668
AC_DRC0_HET             * &66C
AC_DRC0_LET             * &670
AC_DRC0_HKE             * &674
AC_DRC0_LKE             * &678
AC_DRC0_HOPE            * &67C
AC_DRC0_LOPE            * &680
AC_DRC0_HKN             * &684
AC_DRC0_LKN             * &688
AC_DRC0_SFHAT           * &68C
AC_DRC0_SFLAT           * &690
AC_DRC0_SFHRT           * &694
AC_DRC0_SFLRT           * &698
AC_DRC0_MXGHS           * &69C
AC_DRC0_MXGLS           * &6A0
AC_DRC0_MNGHS           * &6A4
AC_DRC0_MNGLS           * &6A8
AC_DRC0_EPSHC           * &6AC
AC_DRC0_EPSLC           * &6B0
AC_DRC0_OPT             * &6B4

AC_DRC1_HHPFC           * &700
AC_DRC1_LHPFC           * &704
AC_DRC1_CTRL            * &708
AC_DRC1_LPFHAT          * &70C
AC_DRC1_LPFLAT          * &710
AC_DRC1_RPFHAT          * &714
AC_DRC1_RPFLAT          * &718
AC_DRC1_LPFHRT          * &71C
AC_DRC1_LPFLRT          * &720
AC_DRC1_RPFHRT          * &724
AC_DRC1_RPFLRT          * &728
AC_DRC1_LRMSHAT         * &72C
AC_DRC1_LRMSLAT         * &730
AC_DRC1_RRMSHAT         * &734
AC_DRC1_RRMSLAT         * &738
AC_DRC1_HCT             * &73C
AC_DRC1_LCT             * &740
AC_DRC1_HKC             * &744
AC_DRC1_LKC             * &748
AC_DRC1_HOPC            * &74C
AC_DRC1_LOPC            * &750
AC_DRC1_HLT             * &754
AC_DRC1_LLT             * &758
AC_DRC1_HKI             * &75C
AC_DRC1_LKI             * &760
AC_DRC1_HOPL            * &764
AC_DRC1_LOPL            * &768
AC_DRC1_HET             * &76C
AC_DRC1_LET             * &770
AC_DRC1_HKE             * &774
AC_DRC1_LKE             * &778
AC_DRC1_HOPE            * &77C
AC_DRC1_LOPE            * &780
AC_DRC1_HKN             * &784
AC_DRC1_LKN             * &788
AC_DRC1_SFHAT           * &78C
AC_DRC1_SFLAT           * &790
AC_DRC1_SFHRT           * &794
AC_DRC1_SFLRT           * &798
AC_DRC1_MXGHS           * &79C
AC_DRC1_MXGLS           * &7A0
AC_DRC1_MNGHS           * &7A4
AC_DRC1_MNGLS           * &7A8
AC_DRC1_EPSHC           * &7AC
AC_DRC1_EPSLC           * &7B0
AC_DRC1_OPT             * &7B4

; Audio analogue registers

ANA_HP_CTRL        * &00
ANA_OL_MIX_CTRL    * &01
ANA_OR_MIX_CTRL    * &02
ANA_EARPIECE_CTRL0 * &03
ANA_EARPIECE_CTRL1 * &04
ANA_LINEOUT_CTRL0  * &05
ANA_LINEOUT_CTRL1  * &06
ANA_MIC1_CTRL      * &07
ANA_MIC2_CTRL      * &08
ANA_LINEIN_CTRL    * &09
ANA_MIX_DAC_CTRL   * &0A
ANA_L_ADCMIX_SRC   * &0B
ANA_R_ADCMIX_SRC   * &0C
ANA_ADC_CTRL       * &0D
ANA_HS_MBIAS_CTRL  * &0E
ANA_APT_REG        * &0F
ANA_OP_BIAS_CTRL0  * &10
ANA_OP_BIAS_CTRL1  * &11
ANA_ZC_VOL_CTRL    * &12
ANA_BIAS_CAL_DATA  * &13
ANA_BIAS_CAL_SET   * &14
ANA_BD_CAL_CTRL    * &15
ANA_HP_PA_CTRL     * &16
ANA_HP_CAL_CTRL    * &17
ANA_RHP_CAL_DAT    * &18
ANA_RHP_CAL_SET    * &19
ANA_LHP_CAL_DAT    * &1A
ANA_LHP_CAL_SET    * &1B
ANA_MDET_CTRL      * &1C
ANA_JM_DET_CTRL    * &1D
ANA_PHOUT_CTRL     * &1E
ANA_PHIN_CTRL      * &1F

; Port controller regs
PH_CFG0 * &FC
PH_DAT * &10C

; CCU regs
PLL_AUDIO_CTRL_REG  * &008
BUS_CLK_GATING_REG2 * &068
AC_DIG_CLK_REG      * &140
BUS_SOFT_RST_REG3   * &2D0

; PRCM regs
AC_PR_CFG_REG * &1C0

        END
