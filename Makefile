# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "Licence").
# You may not use this file except in compliance with the Licence.
#
# You can obtain a copy of the licence at
# RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the Licence file. If applicable, add the
# following below this CDDL HEADER, with the fields enclosed by
# brackets "[]" replaced with your own identifying information:
# Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Portions Copyright 2019 Michael Grunditz
# Portions Copyright 2019 John Ballance
# Use is subject to license terms.
#
#
# Makefile for PINEBOOK HAL
#

COMPONENT = PINEBOOK HAL
TARGET = PINEA64
OBJS = Top Boot Interrupts Timers CLib  SDIO  RTC CLibAsm  Debug  Video USB        NVMemory  KbdScan  UART device util haldebugp atomic DMA Audio
#USBDIR = <Lib$Dir>.USB

HDRS =
CMHGFILE =
CUSTOMRES = custom
CUSTOMROM = custom
ROM_TARGET = custom
LNK_TARGET = custom
AIFDBG    = aif._${TARGET}
GPADBG    = gpa.GPA

include CModule
#ASFLAGS += -PD "VideoInHAL SETL {${VideoInHAL}}"
CCFLAGS = -ff -APCS 3/32bit/nofp/noswst
ASFLAGS += -APCS 3/nofp/noswst   --cpu 8-A.32
#ROM_LIBS += ${USBDIR}.o.MUSBDriver ${USBDIR}.o.EHCIDriver ${USBDIR}.o.USBDriver
CFLAGS += -DVideoInHAL
#CFLAGS     += -DDEBUG_ENABLED


resources:
        @${ECHO} ${COMPONENT}: no resources

rom: aof.${TARGET}
        @${ECHO} ${COMPONENT}: rom module built

_debug: ${GPADBG}
        @echo ${COMPONENT}: debug image built

install_rom: linked.${TARGET}
        ${CP} linked.${TARGET} ${INSTDIR}.${TARGET} ${CPFLAGS}
        @echo ${COMPONENT}: rom module installed

aof.${TARGET}: ${ROM_OBJS_} ${ROM_LIBS} ${DIRS} ${ROM_DEPEND}
        ${LD} -o $@ -aof ${ROM_OBJS_} ${ROM_LIBS}

linked.${TARGET}: aof.${TARGET}
        ${LD} ${LDFLAGS} ${LDLINKFLAGS} -o $@ -bin -base 0xFC000000 aof.${TARGET}

${AIFDBG}: ${ROM_OBJS_} ${ROM_LIBS}
        ${MKDIR} aif
        ${LD} -aif -bin -d -o ${AIFDBG} ${ROM_OBJS_} ${ROM_LIBS}

${GPADBG}: ${AIFDBG}
        ToGPA -s ${AIFDBG} ${GPADBG}

# Dynamic dependencies:
