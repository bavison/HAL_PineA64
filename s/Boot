; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;


        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:HALSize.<HALSize>

        GET     Hdr:MEMM.VMSAv6

        GET     Hdr:Proc
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries


        GET     hdr.PINEA64
        GET     hdr.StaticWS
        GET     hdr.SDRC
        GET     hdr.Interrupts
        GET     hdr.Timers

; This version assumes a RISC OS image starting OSROM_HALSize bytes after us.

; FIQ-based debugger - prints out the PC when the beagleboard/touchbook USER button is pressed
; The code installs itself when HAL_InitDevices is called with R0=123. e.g. SYS "OS_Hardware",123,,,,,,,,0,100
                GBLL FIQDebug
FIQDebug        SETL {FALSE}

                GBLL MoreDebug
MoreDebug       SETL Debug :LAND: {TRUE}

        AREA    |Asm$$Code|, CODE, READONLY, PIC

        EXPORT  rom_checkedout_ok
        EXPORT  RSBReadByte
        EXPORT  RSBWriteByte

        IMPORT  HAL_Base

        IMPORT  RTC_Init
 [ Debug
        IMPORT DebugHALPrint
        IMPORT DebugHALPrintReg
 ]

        MACRO
        CallOSM $entry, $reg
        LDR     ip, [v8, #$entry*4]
        MOV     lr, pc
        ADD     pc, v8, ip
        MEND

rom_checkedout_ok

        ; On entry, v8 -> OS entry table, sb -> board config
        ; Register the attached RAM
        mov a4,sb
        B addramns

addramns
        mov       sb,a4
        DebugTX   "BOOT"

	LDR	a2, =&40800000
        LDR	a3, =&BCFFFFF0; 70000000 ;3FC00000 ;FF000000 ; v4
        ;DebugReg a2, "Adding RAM from "
        ;DebugReg a3, "Adding RAM to "
        MVN     a4, #0
        MOV     a1, #0
	LDR     a4, =&FFFFFFFF
        ADD     sp, a2, #4096 ;

        STR     a1, [sp, #-4]!  ;reference handle (NULL for first call)
	;DebugTX	"Before OS_AddRAM"
        CallOSM OS_AddRAM
        DebugTX  "Added "
        ;SMC      #0
	DebugReg a1, "RAM to OS "


        ;DebugChar v1,v2,71
        MOV     v1, a1
        ;BL      SDMA_Had_POR
        CMP     a1, #0
        MOVNE   a1, #OSStartFlag_RAMCleared :OR: OSStartFlag_POR
        MOVEQ   a1, #OSStartFlag_RAMCleared
	MOV	a1,#2
	;ORR	a1,a1, #OSStartFlag_NoCMOS
        ADRL    a2, HAL_Base + OSROM_HALSize       ; a2 -> RISC OS image
        ADR     a3, HALdescriptor
        MOV     a4, v1
	DebugTX	"Before OS_Start"
        CallOSM OS_Start

HAL_IICBuses               ; tell system no busses
        MOV      a1, #0
        MOV      pc, lr


 EXPORT HALdescriptor
HALdescriptor   DATA
        DCD     HALFlag_NCNBWorkspace
        DCD     HAL_Base - HALdescriptor
        DCD     OSROM_HALSize
        DCD     HAL_EntryTable - HALdescriptor
        DCD     HAL_Entries
        DCD     HAL_WsSize


        MACRO
        HALEntry $name
        ASSERT  (. - HAL_EntryTable) / 4 = EntryNo_$name
        DCD     $name - HAL_EntryTable
        MEND

        MACRO
        NullEntry
        DCD     HAL_Null - HAL_EntryTable
        MEND

        IMPORT   Video_Init
        IMPORT   Interrupt_Init
        IMPORT   Timer_Init

        IMPORT   USB_Init



        IMPORT   VideoDevice_Init


        IMPORT   SDIO_InitDevicesB
        IMPORT   NVMemory_Init
        IMPORT   DMA_InitDevices
        IMPORT   Audio_InitDevices



        IMPORT   HAL_IRQEnable
        IMPORT   HAL_IRQDisable
        IMPORT   HAL_IRQClear
        IMPORT   HAL_IRQSource
        IMPORT   HAL_IRQStatus
        IMPORT   HAL_FIQEnable
        IMPORT   HAL_FIQDisable
        IMPORT   HAL_FIQDisableAll
        IMPORT   HAL_FIQClear
        IMPORT   HAL_FIQSource
        IMPORT   HAL_FIQStatus
        IMPORT   HAL_IRQMax

        IMPORT   HAL_Timers
        IMPORT   HAL_TimerDevice
        IMPORT   HAL_TimerGranularity
        IMPORT   HAL_TimerMaxPeriod
        IMPORT   HAL_TimerSetPeriod
        IMPORT   HAL_TimerPeriod
        IMPORT   HAL_TimerReadCountdown
        IMPORT   HAL_TimerIRQClear

        IMPORT   HAL_CounterRate
        IMPORT   HAL_CounterPeriod
        IMPORT   HAL_CounterRead
        IMPORT   HAL_CounterDelay



        IMPORT   HAL_NVMemoryType
        IMPORT   HAL_NVMemorySize
        IMPORT   HAL_NVMemoryPageSize
        IMPORT   HAL_NVMemoryProtectedSize
        IMPORT   HAL_NVMemoryProtection
        IMPORT   HAL_NVMemoryRead
        IMPORT   HAL_NVMemoryWrite

        IMPORT   HAL_UARTPorts


        IMPORT   HAL_DebugRX
        IMPORT   HAL_DebugTX

        IMPORT   HAL_KbdScanDependencies
        IMPORT   HAL_USBControllerInfo
        IMPORT   HAL_Watchdog

HAL_EntryTable  DATA
        HALEntry HAL_Init

        HALEntry HAL_IRQEnable
        HALEntry HAL_IRQDisable
        HALEntry HAL_IRQClear
        HALEntry HAL_IRQSource
        HALEntry HAL_IRQStatus
        HALEntry HAL_FIQEnable
        HALEntry HAL_FIQDisable
        HALEntry HAL_FIQDisableAll
        HALEntry HAL_FIQClear
        HALEntry HAL_FIQSource
        HALEntry HAL_FIQStatus

        HALEntry HAL_Timers
        HALEntry HAL_TimerDevice
        HALEntry HAL_TimerGranularity
        HALEntry HAL_TimerMaxPeriod
        HALEntry HAL_TimerSetPeriod
        HALEntry HAL_TimerPeriod
        HALEntry HAL_TimerReadCountdown

        HALEntry HAL_CounterRate
        HALEntry HAL_CounterPeriod
        HALEntry HAL_CounterRead
        HALEntry HAL_CounterDelay

        HALEntry HAL_NVMemoryType
        HALEntry HAL_NVMemorySize
        HALEntry HAL_NVMemoryPageSize
        HALEntry HAL_NVMemoryProtectedSize
        HALEntry HAL_NVMemoryProtection
	NullEntry ;       	HALEntry HAL_NVMemoryIICAddress
        HALEntry HAL_NVMemoryRead
        HALEntry HAL_NVMemoryWrite

        HALEntry HAL_IICBuses
        NullEntry ;HALEntry HAL_IICType
        NullEntry ; HAL_IICSetLines
        NullEntry ; HAL_IICReadLines
        NullEntry ;HALEntry HAL_IICDevice
        NullEntry ;HALEntry HAL_IICTransfer
        NullEntry ;HALEntry HAL_IICMonitorTransfer

        NullEntry ; HALEntry HAL_VideoFlybackDevice
        NullEntry ; HALEntry HAL_VideoSetMode
        NullEntry ; HALEntry HAL_VideoWritePaletteEntry
        NullEntry ; HALEntry HAL_VideoWritePaletteEntries
        NullEntry ; HALEntry HAL_VideoReadPaletteEntry
        NullEntry ; HALEntry HAL_VideoSetInterlace
        NullEntry ; HALEntry HAL_VideoSetBlank
        NullEntry ; HALEntry HAL_VideoSetPowerSave
        NullEntry ; HALEntry HAL_VideoUpdatePointer
        NullEntry ; HALEntry HAL_VideoSetDAG
        NullEntry ; HALEntry HAL_VideoVetMode
        NullEntry ; HALEntry HAL_VideoPixelFormats
        NullEntry ; HALEntry HAL_VideoFeatures
        NullEntry ; HALEntry HAL_VideoBufferAlignment
        NullEntry ; HALEntry HAL_VideoOutputFormat

        NullEntry ; HALEntry HAL_MatrixColumns
        NullEntry ; HALEntry HAL_MatrixScan

        NullEntry ; HALEntry HAL_TouchscreenType
        NullEntry ; HALEntry HAL_TouchscreenRead
        NullEntry ; HALEntry HAL_TouchscreenMode
        NullEntry ; HALEntry HAL_TouchscreenMeasure

        NullEntry  ;HALEntry HAL_MachineID

        NullEntry ;HALEntry HAL_ControllerAddress
        NullEntry ;HALEntry HAL_HardwareInfo
        NullEntry ;HALEntry HAL_SuperIOInfo
        HALEntry HAL_PlatformInfo
        NullEntry ; HALEntry HAL_CleanerSpace

        HALEntry HAL_UARTPorts
        NullEntry ;HALEntry HAL_UARTStartUp
        NullEntry ;HALEntry HAL_UARTShutdown
        NullEntry ;HALEntry HAL_UARTFeatures
        NullEntry ;HALEntry HAL_UARTReceiveByte
        NullEntry ;HALEntry HAL_UARTTransmitByte
        NullEntry ;HALEntry HAL_UARTLineStatus
        NullEntry ;HALEntry HAL_UARTInterruptEnable
        NullEntry ;HALEntry HAL_UARTRate
        NullEntry ;HALEntry HAL_UARTFormat
        NullEntry ;HALEntry HAL_UARTFIFOSize
        NullEntry ;HALEntry HAL_UARTFIFOClear
        NullEntry ;HALEntry HAL_UARTFIFOEnable
        NullEntry ;HALEntry HAL_UARTFIFOThreshold
        NullEntry ;HALEntry HAL_UARTInterruptID
        NullEntry ;HALEntry HAL_UARTBreak
        NullEntry ;HALEntry HAL_UARTModemControl
        NullEntry ;HALEntry HAL_UARTModemStatus
        NullEntry ;HALEntry HAL_UARTDevice
        NullEntry ;HALEntry HAL_UARTDefault


        HALEntry HAL_DebugRX
        HALEntry HAL_DebugTX

        NullEntry ; HAL_PCIFeatures
        NullEntry ; HAL_PCIReadConfigByte
        NullEntry ; HAL_PCIReadConfigHalfword
        NullEntry ; HAL_PCIReadConfigWord
        NullEntry ; HAL_PCIWriteConfigByte
        NullEntry ; HAL_PCIWriteConfigHalfword
        NullEntry ; HAL_PCIWriteConfigWord
        NullEntry ; HAL_PCISpecialCycle
        NullEntry ; HAL_PCISlotTable
        NullEntry ; HAL_PCIAddresses

        NullEntry ;HALEntry HAL_PlatformName
        NullEntry
        NullEntry

        HALEntry HAL_InitDevices

        HALEntry HAL_KbdScanDependencies ;HAL_KbdScanDependencies
        NullEntry ;HALEntry HAL_KbdScanSetup
        NullEntry ;HALEntry HAL_KbdScan
        NullEntry ;HALEntry HAL_KbdScanFinish
        ;HALEntry HAL_KbdScanInterrupt

        NullEntry ;HALEntry HAL_PhysInfo

        HALEntry HAL_Reset



        HALEntry HAL_IRQMax

        HALEntry HAL_USBControllerInfo

        NullEntry ; HAL_VideoRender

        NullEntry ; HAL_USBPortPower
        NullEntry ; HAL_USBPortIRQStatus
        NullEntry ; HAL_USBPortIRQClear



        HALEntry HAL_TimerIRQClear
        NullEntry ; HALEntry HAL_TimerIRQStatus

        NullEntry ; HALEntry HAL_ExtMachineID

        NullEntry ; HALEntry HAL_VideoFramestoreAddress
        NullEntry ; HALEntry HAL_VideoRender
        NullEntry ; HALEntry HAL_VideoStartupMode
        NullEntry ; HALEntry HAL_VideoPixelFormatList
        NullEntry ; HALEntry HAL_VideoIICOp

        HALEntry HAL_Watchdog


HAL_Entries     * (.-HAL_EntryTable)/4


;--------------------------------------------------------------------------------------


HAL_Init
        Entry   "v1-v3"

        STR     a2, NCNBWorkspace         ;??????
        STR     a2, NCNBAllocNext

        BL      SetUpOSEntries



	MOV     a1, #0
        LDR     a2, =A64_UART_0
        MOV     a3, #A64_UART_0_SIZE
        CallOS  OS_MapInIO

	MOV	a2,#'Y'
	STR	a2,[a1]
	MOV     v4,a1

        B       continue

	MOV     a1, #0
        LDR     a2, =A64_SRAM_A1
        MOV     a3, #A64_SRAM_A1_SIZE
        CallOS  OS_MapInIO
        STR     a1, IntSRAM_Log

        MOV     v2,a1
        MOV     a4,sb

writesrams
        MOV     sb,a4
        MOV     a1,v2
        MOV     a2, #HAL_WsSize ;BoardConfig_Size
10      SUBS    a2, a2, #4
        LDR     a3, [a1, a2]

        STR     a3, [sb, a2]
        BGT     %BT10
        MOV     a4,sb
        MOV     a1,a2

       B continue

continue


        ;MOV     sb,a4

        MOV     a1,v4
         MOV	a2,#'Q'
	STR	a2,[a1]

       ; LDR	a3,[sb, #:INDEX:DebugUART]
;	CMP	a3,#0
;	BEQ	printloop


	STR	a1,[sb, #:INDEX:DebugUART]
;	STR	a1,[sb,#BoardConfig_HALUART]

	B	contrprint

	B	endprintloop
printloop
	Pull 	"a1"
	;LDR	a1, =BoardConfig_DebugUART
	MOV	a2,#'P'
	STR	a2,[a1]
        B	printloop
endprintloop
	Pull	"a1"
	MOV	a2,#'Y'
	STR	a2,[a1]
contrprint


	DebugTX "MMU up , runningA"


		MOV	a3,#0


        DebugTX "1"
	MOV	a3,#0
	MOV     a1, #0
        LDR     a2, =A64_USB_EHCI0_OHCI0
        MOV     a3, #A64_USB_EHCI0_OHCI0_SIZE
        CallOS  OS_MapInIO

        STR     a1, USB_Host_Log
	DebugReg a1,"USB LOG: "

	MOV	a3,#0
	MOV     a1, #0
        LDR     a2, =A64_USB_OTG_EHCI_OHCI
        MOV     a3, #A64_USB_OTG_EHCI_OHCI_SIZE
        CallOS  OS_MapInIO

        STR     a1, USB_OTGHost_Log
	DebugReg a1,"OTG Host LOG: "

	MOV	a3,#0
	MOV     a1, #0
        LDR     a2, =A64_USB_OTG_Device
        MOV     a3, #A64_USB_OTG_Device_SIZE
        CallOS  OS_MapInIO

        STR     a1, USB_OTGDev_Log
	DebugReg a1,"USB Device LOG: "

	MOV	a3,#0

	MOV     a1, #0
        LDR     a2, =A64_TIMER
        MOV     a3, #A64_TIMER_SIZE
        CallOS  OS_MapInIO
        STR     a1, Timers_Log
	DebugReg a1, "Timers_Log: "

	MOV     a1, #0
        LDR     a2, =A64_PORT_CTRL
        MOV     a3, #A64_PORT_CTRL_SIZE
        CallOS  OS_MapInIO
        STR     a1, PC_Log
        DebugReg a1,"PC_Log boot: "

	MOV     a1, #0
        LDR     a2, =A64_SMHC_0
        MOV     a3, #A64_SMHC_0_SIZE
        CallOS  OS_MapInIO
        STR     a1, SDIO_Log
        DebugReg a1,"SDIO_Log boot: "

        MOV     a1, #0
        LDR     a2, =A64_CCU
        MOV     a3, #A64_CCU_SIZE
        CallOS  OS_MapInIO
        STR     a1, CCU_Log
        DebugReg    a1,"CCU_Log from boot"

        MOV     a1, #0
        LDR     a2, =A64_R_RSB
        MOV     a3, #A64_R_RSB_SIZE
        CallOS  OS_MapInIO
        STR     a1, RSB_Log
        DebugReg    a1,"RSB_Log from boot"
	MOV	a3,#0

        MOV     a1, #0
        LDR     a2, =A64_PWM
        MOV     a3, #A64_PWM_SIZE
        CallOS  OS_MapInIO
        STR     a1, PWM_Log
        DebugReg    a1,"PWM_Log from boot"
	MOV	a3,#0

	MOV     a1, #0
        LDR     a2, =A64_GIC
        LDR     a3, =A64_GIC_SIZE
        CallOS  OS_MapInIO
        STR     a1, IRQ_Log
	DebugReg a1,"IRQ_Log: "



	MOV	a3,#0

	MOV     a1, #0
        LDR     a2, =A64_DE
        MOV     a3, #A64_DE_SIZE
        CallOS  OS_MapInIO
        STR     a1, DE_Log

	MOV     a1, #0
        LDR     a2, =A64_RTC
        MOV     a3, #A64_RTC_SIZE
        CallOS  OS_MapInIO
        STR     a1, RTC_Log

	MOV     a1, #0
        LDR     a2, =A64_R_TWI
        MOV     a3, #A64_R_TWI_SIZE
        CallOS  OS_MapInIO
        STR     a1, R_TWI_Log

	MOV     a1, #0
        LDR     a2, =A64_TWI_0
        MOV     a3, #A64_TWI_0_SIZE
        CallOS  OS_MapInIO
        STR     a1, TWI0_Log

	MOV     a1, #0
        LDR     a2, =A64_TWI_1
        MOV     a3, #A64_TWI_1_SIZE
        CallOS  OS_MapInIO
        STR     a1, TWI1_Log

	MOV     a1, #0
        LDR     a2, =A64_TWI_2
        MOV     a3, #A64_TWI_2_SIZE
        CallOS  OS_MapInIO
        STR     a1, TWI2_Log

	MOV     a1, #0
        LDR     a2, =A64_TIMER
        MOV     a3, #A64_TIMER_SIZE
        CallOS  OS_MapInIO
        STR     a1, TIMER_Log

	MOV     a1, #0
        LDR     a2, =A64_R_PRCM
        MOV     a3, #A64_R_PRCM_SIZE
        CallOS  OS_MapInIO
        STR     a1, PRCM_Log





 [ Debug
        DebugTX "HAL_Init"
       ; DebugTime a1, "@ "
 ]
            BL	Timer_Init

halinitns

       ; BL      CPUClk_PreInit ; Go fast!
	BL      Interrupt_Init

 [ MoreDebug
        DebugTX "Video_Init"
 ]
        BL      Video_Init ; Uses GPTIMER2

 [ MoreDebug
        DebugTX "USB_Init"
 ]
        BL      USB_Init ; Uses GPTIMER2

 [ MoreDebug
        DebugTX "NVMemory_Init"
 ]
      ;  BL      NVMemory_Init

 [ MoreDebug
        DebugTX "Timer_Init"
 ]
        ;BL      Timer_Init ; Re-inits timers

 [ MoreDebug
        DebugTX "Interrupt_Init"
 ]
       ; BL      Interrupt_Init

 [ MoreDebug
        DebugTX "GPMC_Init"
 ]

        ; Mark HAL as initialised
        STR     pc, HALInitialised ; Any nonzero value will do

;        DebugTime a1, "HAL initialised @ "

        EXIT


; Dodgy phys->log conversion using the mapped in IO ranges
; In/out: a1 = offset into sb of address to get/put
; Out: a2 = log addr
; Corrupts a3

; NOT USED PINEBOOK
phys2log
        LDR     a3, [sb, a1]
        CMP     a3, #0 ; Null pointers are valid; ignore them
        MOVEQ   a2, #0
        MOVEQ   pc, lr
;        SUB     a2, a3, #IOMEMSTART ;L3_Control
        ;CMP     a2, #IOMEMSIZE ;L3_Size
        ;LDRLO   a3, L3_Log
;	LDR	a3, L3_Log
        ;BLO     %FT10
       ; SUB     a2, a3, #L4_Per
       ; CMP     a2, #L4_Per_Size
       ; LDRLO   a3, L4_Per_Log
       ; BLO     %FT10
       ; SUB     a2, a3, #L4_Core
        ;CMP     a2, #L4_Core_Size
        ;LDRLO   a3, L4_Core_Log
        ;SUBHI   a2, a2, #L4_Wakeup-L4_Core
        ;LDRHI   a3, L4_Wakeup_Log
10
        ADD     a2, a2, a3
        STR     a2, [sb, a1]
        MOV     pc, lr

; Initialise and relocate the entry table.
SetUpOSEntries  ROUT
        STR     a1, OSheader
        LDR     a2, [a1, #OSHdr_NumEntries]
        CMP     a2, #HighestOSEntry+1
        MOVHI   a2, #HighestOSEntry+1

        ADR     a3, OSentries
        LDR     a4, [a1, #OSHdr_Entries]
        ADD     a4, a4, a1

05      SUBS    a2, a2, #1
        LDR     ip, [a4, a2, LSL #2]
        ADD     ip, ip, a4
        STR     ip, [a3, a2, LSL #2]
        BNE     %BT05
        ; Fall through

HAL_Null
        MOV     pc, lr

HAL_InitDevices

        Entry   "v1-v3"
;        DebugTime a1, "HAL_InitDevices @ "

        BL      RTC_Init
        BL      SDIO_InitDevicesB

        BL      DMA_InitDevices ; Maps in DMA regs needed by VideoDevice_Init
        BL      VideoDevice_Init
        BL      Audio_InitDevices

        MRC     p15, 0,v1, c1, c0, 0; Read SCTLR
	BIC	v1,v1,#&180000
	MCR     p15, 0,v1, c1, c0, 0; Write SCTLR

        EXIT

; a0=0  turn off power
; a0=1 hard reset
HAL_Reset      ROUT
; hardware reset is achieved by writing the watchog timer
        Entry
 DebugReg a1,"HAL_Reset hardreset: "
        teq      a1, #0
        beq      %ft1
        ldr      a2, TIMER_Log
        mov      a3, #&1
        str      a3, [a2,#WD0_CFG]          ; set for full reset
        str      a3, [a2,#WD0_MODE]         ; enable with 1/2 sec timeout
        DebugTX "HAL_Reset hardreset is coming!"
        B        .                          ; better stop here
        EXIT
1
;        LDR   a2,RSB_Log
        DebugTX  "HAL_Reset Poweroff start:"
;        DebugReg a2,"RSB_Log: "
;        MOV   a1,#&2d<<16
;        STR   a1,[a2,#&30]                   ; set slave address
;        MOV   a1,#&4e
;        STR   a1,[a2,#&2c]                   ; command, writebyte
;        MOV   a1,#&32                        ; use reg &32
;        STR   a1,[a2,#&10]                   ; reg for poweroff
;        MOV   a1,#&80                        ; bit7 set = poweroff
;        STR   a1,[a2,#&1c]                   ; 1 data byte
;        MOV   a1,#&80
;        STR   a1,[a2,#0]                     ; start command
        mov     a1, #&80
        mov     a2, #&32
        bl      RSBWriteByte
        DebugTX "HAL_Poweroff possibly failed!"
        B        .                          ; better stop here
        EXIT




HAL_PlatformInfo
        MOV     ip, #2_11000     ; no podules,no PCI cards,no multi CPU,soft off,and soft ROM
        STR     ip, [a2]
        MOV     ip, #2_11111    ; mask of valid bits
        STR     ip, [a3]
        MOV     pc, lr


errormap
	DebugTX "Could not map in "
	MOV	 pc,lr

; on entry a2= register to read
; on exit  a1=content
; assumes sb valid
RSBReadByte
        Entry   "a2,a3"
        LDR     a3, RSB_Log
        DebugReg a2,"RSB_Read: "
        MOV     a1, #&2d<<16
        STR     a1, [a3,#&30]                   ; set slave address
        MOV     a1, #&8b
        STR     a1, [a3,#&2c]                   ; command, readbyte
        MOV     a1, #&32                        ; use reg &32
        STR     a2, [a3,#&10]                   ; reg
        MOV     a1, #&80
        STR     a1, [a3,#0]                     ; start command
1       ldr     a1, [a3,#0]
        tst     a1, #&80
        bne     %BT1
        ldr     a1, [a3, #&1c]
        EXIT

; on entry a2= register to write
;          a1= byte to write
; assumes sb valid
RSBWriteByte
        Entry   "a1-a4"
        LDR     a3, RSB_Log
        DebugReg a2,"RSB_Write: "
        DebugReg a1,"Byte: "
        MOV     a4, #&2d<<16
        STR     a4, [a3,#&30]                   ; set slave address
        MOV     a4, #&4e
        STR     a4, [a3,#&2c]                   ; command, writebyte
        STR     a2, [a3,#&10]                   ; reg for poweroff
        STR     a1, [a3,#&1c]                   ; 1 data byte
        MOV     a4, #&80
        STR     a4, [a3,#0]                     ; start command
        EXIT





	;LTORG
	;GET     SMCSecure.s

   LTORG

        EXPORT  vtophys
vtophys
        CallOS  OS_LogToPhys, tailcall

        EXPORT  mapinio
mapinio
        CallOS  OS_MapInIO, tailcall
        END
